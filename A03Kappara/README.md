# A03 Assignment

An Indian Online Food ordering website using node.js, bootstrap, guestbook.

## How to use

Open a command window in your c:\44563\A03Kappara folder.

Run npm install to install all the dependencies in the package.json file.

Run node gbapp.js to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> node gbapp.js
```

Point your browser to 'http://127.0.0.1:8081/'.

## Reference

http://javabeginnerstutorial.com/javascript-2/create-simple-chat-application-using-node-js-express-js-socket-io/

http://socket.io/get-started/chat/

https://github.com/socketio/socket.io

http://socket.io/

