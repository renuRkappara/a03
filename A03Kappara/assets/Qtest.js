QUnit.test('Testing Multiplication function with four sets of inputs', function (assert) {
    assert.throws(function () { calculate(); }, new Error("only numbers are allowed"), 'Passing in array correctly raises an Error');
    assert.strictEqual(calculate(), 2, 'All positive numbers');
    assert.strictEqual(calculate(), -1, 'All are negative numbers');
});
